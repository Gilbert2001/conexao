
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gilberto Filho
 */
public class FabricaConexao {

    public static Connection obterConexaoPostgreeSql() throws SQLException, ClassNotFoundException{
        Connection c;
        Class.forName("org.postgresql.Driver");
        c = DriverManager.getConnection("jdbc:postgresql://192.168.101.55/db_aula","aula","aula");
        return c;
    }
    public static Connection obterConexaoMySql() throws SQLException, ClassNotFoundException{
        Connection c;
        Class.forName("com.mysql.jdbc.Driver");
        c = DriverManager.getConnection("jdbc:mysql://192.168.101.55/db_aula","aula","aula");
        return c;
    }
}
