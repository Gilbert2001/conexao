
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aluno
 */
public class TestarMySql {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Connection c = FabricaConexao.obterConexaoMySql();
        PreparedStatement ps = c.prepareStatement("select * from tb_teste");
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            System.out.println(rs.getString(1));
        }
    }
}
