
import java.awt.image.RescaleOp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aluno
 */
public class ConexaoPrincipal {
    
    public static void main(String[] args) {
        try {
            System.out.println("oi");
            Connection c = FabricaConexao.obterConexaoPostgreeSql();
            String sql = "select * from tb_teste";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                System.out.println(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexaoPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
